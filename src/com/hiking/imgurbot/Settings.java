package com.hiking.imgurbot;

/**
 * App global settings.
 *
 * @author Created by hiking on 2017/6/19.
 */
class Settings {

	static final float WARNING_THRESHOLD = .50f;
	static final float DANGER_THRESHOLD = .20f;

	static class CustomKeySource {

		String slackTitle;
		String[] keys;

		CustomKeySource(String slackTitle, String[] keys) {
			this.slackTitle = slackTitle;
			this.keys = keys;
		}
	}

	/**
	 * Custom Imgur key source.
	 *
	 * @return Custom keys. Return null to fetch keys from Dcard config API.
	 */
	static CustomKeySource getCustomKeySource() {
		return null;
	}
}
