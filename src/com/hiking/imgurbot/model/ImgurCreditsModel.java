package com.hiking.imgurbot.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ImgurCreditsModel {

	public Data data;
	public boolean success;
	public int status;

	public class Data {

		@SerializedName("UserRemaining") public int userRemaining;
		@SerializedName("ClientRemaining") public int clientRemaining;
		@SerializedName("UserReset") public int userReset;
		@SerializedName("UserLimit") public int userLimit;
		@SerializedName("ClientLimit") public int clientLimit;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}
