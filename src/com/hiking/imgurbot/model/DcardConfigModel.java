package com.hiking.imgurbot.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Imgur client ids config from Dcard
 *
 * @author Created by hiking on 2016/9/23.
 */
public class DcardConfigModel {

	@SerializedName("android_imgur_keys") public String[] androidImgurKeys;
	@SerializedName("ios_imgur_keys") public String[] iosImgurKeys;
	@SerializedName("web_imgur_keys") public String[] webImgurKeys;

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}
