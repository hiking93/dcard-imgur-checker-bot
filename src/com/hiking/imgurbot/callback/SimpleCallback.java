package com.hiking.imgurbot.callback;

/**
 * Callback for empty response
 *
 * @author Created by hiking on 2016/9/23.
 */
public abstract class SimpleCallback extends BaseCallback {

	public abstract void onSuccess();
}
