package com.hiking.imgurbot.callback;

import com.hiking.imgurbot.model.DcardConfigModel;

/**
 * Callback for Dcard config {@link DcardConfigModel}
 *
 * @author Created by hiking on 2016/9/23.
 */
public abstract class ConfigCallback extends BaseCallback {

	public abstract void onSuccess(DcardConfigModel config);
}
