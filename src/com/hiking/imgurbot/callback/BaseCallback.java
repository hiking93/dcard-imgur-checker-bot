package com.hiking.imgurbot.callback;

/**
 * Base callback for some task might fail
 *
 * @author Created by hiking on 2016/9/23.
 */
abstract class BaseCallback {

	public abstract void onFail(String errorMessage);
}
