package com.hiking.imgurbot.callback;

import com.hiking.imgurbot.model.ImgurCreditsModel;

/**
 * Callback for Imgur credits {@link ImgurCreditsCallback}
 *
 * @author Created by hiking on 2016/9/23.
 */
public abstract class ImgurCreditsCallback extends BaseCallback {

	public abstract void onSuccess(ImgurCreditsModel config);
}
