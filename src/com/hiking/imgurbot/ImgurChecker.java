package com.hiking.imgurbot;

import com.hiking.imgurbot.callback.ConfigCallback;
import com.hiking.imgurbot.callback.ImgurCreditsCallback;
import com.hiking.imgurbot.callback.SimpleCallback;
import com.hiking.imgurbot.model.DcardConfigModel;
import com.hiking.imgurbot.model.ImgurCreditsModel;
import com.hiking.imgurbot.module.DcardHelper;
import com.hiking.imgurbot.module.ImgurHelper;
import com.hiking.imgurbot.module.SlackHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Imgur client remain checker
 *
 * @author Created by hiking on 2016/9/23.
 */
class ImgurChecker {

	private enum CreditStatus {
		GOOD, WARNING, DANGER, NOT_FOUND
	}

	private Object mKeySource;

	private Map<String, Object> mImgurCredits;
	private int mCallbackCount, mCallbackSize;

	void run() {
		mCallbackCount = 0;
		mImgurCredits = new HashMap<>();

		Settings.CustomKeySource customKeys = Settings.getCustomKeySource();
		// noinspection ConstantConditions
		if (customKeys != null) {
			runCustom(customKeys);
		} else {
			runDcardConfig();
		}
	}

	private void runCustom(Settings.CustomKeySource keySource) {
		mKeySource = keySource;
		mCallbackSize = keySource.keys.length;

		for (String imgurKey : keySource.keys) {
			System.out.println("Checking key: " + imgurKey);
			getKeyDetail(imgurKey);
		}
	}

	private void runDcardConfig() {
		DcardHelper.getConfig(new ConfigCallback() {

			@Override
			public void onSuccess(DcardConfigModel config) {
				System.out.println("Config fetched: " + config);
				mKeySource = config;

				mCallbackSize = config.androidImgurKeys.length;
				mCallbackSize += config.iosImgurKeys.length;
				mCallbackSize += config.webImgurKeys.length;

				for (String imgurKey : config.androidImgurKeys) {
					System.out.println("Checking key: " + imgurKey);
					getKeyDetail(imgurKey);
				}
				for (String imgurKey : config.iosImgurKeys) {
					System.out.println("Checking key: " + imgurKey);
					getKeyDetail(imgurKey);
				}
				for (String imgurKey : config.webImgurKeys) {
					System.out.println("Checking key: " + imgurKey);
					getKeyDetail(imgurKey);
				}
			}

			@Override
			public void onFail(String errorMessage) {
				System.err.println("getConfig failed: " + errorMessage);
			}
		});
	}

	private void getKeyDetail(final String imgurKey) {
		ImgurHelper.getCredits(imgurKey, new ImgurCreditsCallback() {

			@Override
			public void onSuccess(ImgurCreditsModel config) {
				System.out.println("Got credits for key " + imgurKey + ": " + config);

				mImgurCredits.put(imgurKey, config);

				mCallbackCount++;

				if (mCallbackCount >= mCallbackSize) {
					onGetCreditsFinished();
				}
			}

			@Override
			public void onFail(String errorMessage) {
				System.err.println("getKeyDetail failed: " + errorMessage);

				mImgurCredits.put(imgurKey, errorMessage);

				mCallbackCount++;
				if (mCallbackCount >= mCallbackSize) {
					onGetCreditsFinished();
				}
			}
		});
	}

	private void onGetCreditsFinished() {
		if (mKeySource instanceof Settings.CustomKeySource) {
			onFinished((Settings.CustomKeySource) mKeySource);
		} else if (mKeySource instanceof DcardConfigModel) {
			onFinished((DcardConfigModel) mKeySource);
		} else {
			throw new IllegalStateException("Unknown key source type.");
		}
	}

	private void onFinished(Settings.CustomKeySource keySource) {
		// Build attachments and post to Slack
		List<SlackHelper.Attachment> attachments = new ArrayList<>();
		attachments.addAll(getSlackAttachments("Key", keySource.keys));
		postToSlack(SlackHelper.PATH_BOT, keySource.slackTitle, attachments);
	}

	private void onFinished(DcardConfigModel config) {
		CreditStatus androidStatus = getFirstKeyStatus(config.androidImgurKeys);
		CreditStatus iosStatus = getFirstKeyStatus(config.iosImgurKeys);
		CreditStatus webStatus = getFirstKeyStatus(config.webImgurKeys);

		// Build path & title according to platform statuses
		String slackPath;
		String title;
		if (androidStatus == CreditStatus.NOT_FOUND || iosStatus == CreditStatus.NOT_FOUND ||
				webStatus == CreditStatus.NOT_FOUND) {
			slackPath = SlackHelper.PATH_DEVELOPER;
			title = "找不到 Imgur 金鑰設定耶，config 有改？";
		} else if (androidStatus == CreditStatus.DANGER || iosStatus == CreditStatus.DANGER ||
				webStatus == CreditStatus.DANGER) {
			slackPath = SlackHelper.PATH_DEVELOPER;
			title = "剛剛看了一下 Imgur 上傳流量，感覺該換一下金鑰。";
		} else if (androidStatus == CreditStatus.WARNING || iosStatus == CreditStatus.WARNING ||
				webStatus == CreditStatus.WARNING) {
			slackPath = SlackHelper.PATH_BOT;
			title = "剛剛看了一下 Imgur 上傳流量，可能需要注意一下剩餘量。";
		} else {
			slackPath = SlackHelper.PATH_BOT;
			title = "照慣例檢查了一下 Imgur 上傳流量，看起來還可以。";
		}

		// Build attachments and post to Slack
		List<SlackHelper.Attachment> attachments = new ArrayList<>();
		attachments.addAll(getSlackAttachments("Android", config.androidImgurKeys));
		attachments.addAll(getSlackAttachments("iOS", config.iosImgurKeys));
		attachments.addAll(getSlackAttachments("Web", config.webImgurKeys));
		postToSlack(slackPath, title, attachments);
	}

	private CreditStatus getFirstKeyStatus(String[] keys) {
		if (keys.length > 0) {
			String key = keys[0];
			Object keyStatusResult = mImgurCredits.get(key);
			if (keyStatusResult instanceof ImgurCreditsModel) {
				ImgurCreditsModel.Data credits = ((ImgurCreditsModel) keyStatusResult).data;
				float ratio = credits.clientRemaining / (float) credits.clientLimit;
				if (ratio <= Settings.DANGER_THRESHOLD) {
					return CreditStatus.DANGER;
				} else if (ratio <= Settings.WARNING_THRESHOLD) {
					return CreditStatus.WARNING;
				} else {
					return CreditStatus.GOOD;
				}
			} else {
				return CreditStatus.WARNING;
			}
		} else {
			return CreditStatus.NOT_FOUND;
		}
	}

	/**
	 * Post attachments to Slack
	 *
	 * @param attachments The attachments to post
	 */
	private void postToSlack(String path, String title, List<SlackHelper.Attachment> attachments) {
		SlackHelper.post(path, title, attachments, new SimpleCallback() {

			@Override
			public void onSuccess() {
				System.out.println("Posted to Slack.");
			}

			@Override
			public void onFail(String errorMessage) {
				System.err.println("postToSlack failed: " + errorMessage);
			}
		});
	}

	/**
	 * Get slack attachments to describe the keys for a platform
	 *
	 * @param platformName Platform name for string building
	 * @param imgurKeys    Keys for the platform
	 * @return Each Slack attachments for a single key
	 */
	private List<SlackHelper.Attachment> getSlackAttachments(String platformName,
	                                                         String[] imgurKeys) {
		List<SlackHelper.Attachment> attachments = new ArrayList<>();
		for (String key : imgurKeys) {
			if (mImgurCredits.get(key) instanceof ImgurCreditsModel) {
				ImgurCreditsModel.Data credits = ((ImgurCreditsModel) mImgurCredits.get(key)).data;
				float ratio = credits.clientRemaining / (float) credits.clientLimit;
				SlackHelper.Attachment attachment =
						new SlackHelper.Attachment(getCreditColor(credits),
								String.format("%s `%s` 剩下 *%d/%d* (%.1f%%).\n", platformName, key,
										credits.clientRemaining, credits.clientLimit, ratio * 100));
				attachments.add(attachment);
			} else if (mImgurCredits.get(key) instanceof String) {
				String errorMessage = ((String) mImgurCredits.get(key));
				SlackHelper.Attachment attachment =
						new SlackHelper.Attachment(SlackHelper.Attachment.COLOR_DANGER,
								String.format("%s `%s` 檢查時發生錯誤：%s\n", platformName, key,
										errorMessage));
				attachments.add(attachment);
			}
		}
		return attachments;
	}

	/**
	 * Get slack display color for a credit status
	 *
	 * @param credits The credit status to display
	 * @return Color string for slack attachment
	 */
	private String getCreditColor(ImgurCreditsModel.Data credits) {
		float ratio = credits.clientRemaining / (float) credits.clientLimit;
		if (ratio > Settings.WARNING_THRESHOLD) {
			return SlackHelper.Attachment.COLOR_GOOD;
		} else if (ratio > Settings.DANGER_THRESHOLD) {
			return SlackHelper.Attachment.COLOR_WARNING;
		} else {
			return SlackHelper.Attachment.COLOR_DANGER;
		}
	}
}
