package com.hiking.imgurbot.module;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.hiking.imgurbot.callback.SimpleCallback;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Helper for Slack webhook requests
 *
 * @author Created by hiking on 2016/9/23.
 */
public class SlackHelper {

	private static final String BASE_SCHEME = "https";
	private static final String BASE_URL = "hooks.slack.com";
	private static final String BASE_PATH = "services";

	public static final String PATH_HIKING = "/T03180XEC/B2JGU1AD6/sEYFRRHDdw30NkEXOyL2bzgr";
	public static final String PATH_GENERAL = "T03180XEC/B4QLP354N/nqMiw9npTaMNeW8ebVRoFDyv";
	public static final String PATH_DEVELOPER = "/T03180XEC/B4U156G1E/naJM6tK5XkRCgtZxZSEH5bTm";
	public static final String PATH_BOT = "T03180XEC/B2F3K9T28/LM217zrjBiWRAX0Vx1ctOonC";

	private static class SlackPostBody {

		@SuppressWarnings("unused") String text = "Checked imgur rates:";
		List<Attachment> attachments;

		SlackPostBody(String title, List<Attachment> attachments) {
			this.text = title;
			this.attachments = attachments;
		}
	}

	public static class Attachment {

		public static final String COLOR_GOOD = "good";
		public static final String COLOR_WARNING = "warning";
		public static final String COLOR_DANGER = "danger";

		String color;
		String text;
		@SuppressWarnings("unused") @SerializedName("mrkdwn_in") String[] markdownIn = {"text"};

		public Attachment(String color, String text) {
			this.color = color;
			this.text = text;
		}
	}

	private static HttpUrl.Builder getBaseUrlBuilder() {
		return new HttpUrl.Builder().scheme(BASE_SCHEME).host(BASE_URL).addPathSegment(BASE_PATH);
	}

	public static void post(String path, String title, List<Attachment> attachments,
	                        SimpleCallback callback) {
		HttpUrl.Builder urlBuilder = getBaseUrlBuilder().addEncodedPathSegments(path);

		SlackPostBody postBody = new SlackPostBody(title, attachments);
		String postBodyString = new Gson().toJson(postBody);

		Request request = new Request.Builder().url(urlBuilder.build())
				.post(RequestBody.create(getJsonMediaType(), postBodyString)).build();
		NetworkHelper.getClient().newCall(request).enqueue(new Callback() {

			@Override
			public void onResponse(Call call, Response response) throws IOException {
				if (!response.isSuccessful()) {
					throw new IOException("Unexpected code " + response);
				}
				if (callback != null) {
					callback.onSuccess();
				}
			}

			@Override
			public void onFailure(Call call, IOException e) {
				e.printStackTrace();
				if (callback != null) {
					callback.onFail(e.getLocalizedMessage());
				}
			}
		});
	}

	private static MediaType getJsonMediaType() {
		return MediaType.parse("application/json; charset=utf-8");
	}
}
