package com.hiking.imgurbot.module;

import okhttp3.OkHttpClient;

/**
 * Manage http client
 * Created by hiking on 2016/10/3.
 */
class NetworkHelper {

	private static OkHttpClient mClient;

	static OkHttpClient getClient() {
		if (mClient == null || mClient.dispatcher().executorService().isShutdown()) {
			mClient = new OkHttpClient();
			mClient.dispatcher().setIdleCallback(() -> {
				System.out.println("Shutting down network executor service.");
				mClient.dispatcher().executorService().shutdown();
			});
		}
		return mClient;
	}
}
