package com.hiking.imgurbot.module;

import com.google.gson.Gson;
import com.hiking.imgurbot.callback.ImgurCreditsCallback;
import com.hiking.imgurbot.model.ImgurCreditsModel;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Helper for Imgur API requests
 *
 * @author Created by hiking on 2016/9/23.
 */
public class ImgurHelper {

	private static final String BASE_SCHEME = "https";
	private static final String BASE_URL = "api.imgur.com";
	private static final String BASE_PATH = "3";

	private static final String PATH_CREDITS = "credits";

	private static HttpUrl.Builder getBaseUrlBuilder() {
		return new HttpUrl.Builder().scheme(BASE_SCHEME).host(BASE_URL).addPathSegment(BASE_PATH);
	}

	public static void getCredits(String clientId, ImgurCreditsCallback callback) {
		HttpUrl.Builder urlBuilder = getBaseUrlBuilder().addPathSegment(PATH_CREDITS);
		Request request = new Request.Builder().url(urlBuilder.build())
				.addHeader("Authorization", "Client-ID " + clientId).build();
		NetworkHelper.getClient().newCall(request).enqueue(new Callback() {

			@Override
			public void onResponse(Call call, Response response) throws IOException {
				if (!response.isSuccessful()) {
					throw new IOException("Unexpected code " + response);
				}

				if (callback != null) {
					try {
						ImgurCreditsModel creditsModel = new Gson()
								.fromJson(response.body().charStream(), ImgurCreditsModel.class);
						callback.onSuccess(creditsModel);
					} catch (Exception e) {
						e.printStackTrace();
						callback.onFail(e.getLocalizedMessage());
					}
				}
			}

			@Override
			public void onFailure(Call call, IOException e) {
				e.printStackTrace();
				if (callback != null) {
					callback.onFail(e.getLocalizedMessage());
				}
			}
		});
	}
}
