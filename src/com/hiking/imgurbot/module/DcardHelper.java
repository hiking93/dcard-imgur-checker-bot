package com.hiking.imgurbot.module;

import com.google.gson.Gson;
import com.hiking.imgurbot.callback.ConfigCallback;
import com.hiking.imgurbot.model.DcardConfigModel;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Helper for Dcard API requests
 *
 * @author Created by hiking on 2016/9/23.
 */
public class DcardHelper {

	private static final String BASE_SCHEME = "https";
	private static final String BASE_URL = "www.dcard.tw";
	private static final String BASE_PATH = "_api";

	private static final String PATH_CONFIG = "configs";

	private static HttpUrl.Builder getBaseUrlBuilder() {
		return new HttpUrl.Builder().scheme(BASE_SCHEME).host(BASE_URL).addPathSegment(BASE_PATH);
	}

	public static void getConfig(ConfigCallback callback) {
		HttpUrl.Builder urlBuilder = getBaseUrlBuilder().addPathSegment(PATH_CONFIG);
		urlBuilder.addQueryParameter("android_imgur_keys", null);
		urlBuilder.addQueryParameter("ios_imgur_keys", null);
		urlBuilder.addQueryParameter("web_imgur_keys", null);

		Request request = new Request.Builder().url(urlBuilder.build()).build();
		NetworkHelper.getClient().newCall(request).enqueue(new Callback() {

			@Override
			public void onResponse(Call call, Response response) throws IOException {
				if (!response.isSuccessful()) {
					throw new IOException("Unexpected code " + response);
				}

				if (callback != null) {
					try {
						callback.onSuccess(new Gson()
								.fromJson(response.body().charStream(), DcardConfigModel.class));
					} catch (Exception e) {
						e.printStackTrace();
						callback.onFail(e.toString());
					}
				}
			}

			@Override
			public void onFailure(Call call, IOException e) {
				e.printStackTrace();
				if (callback != null) {
					callback.onFail(e.toString());
				}
			}
		});
	}
}
